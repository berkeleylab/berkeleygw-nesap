BGW
========
BerkeleyGW is a material science application that predicts the excited-state properties of a wide range of materials from molecules and nanostuctures to crystals - including systems with defects and complex interfaces. 
The code is written in FORTRAN 2003 and utilizes an MPI+OpenMP programming style.

This directory contains one full-code benchmark (in the small directory) and a number of kernels in the kernels directory.


-------------------------
Here is a description of what is contained:

source.gz - The source code for the full app gzipped

small - The small example. This utilizes the sigma.cplx.x executable from the compiled source code

kernels/gpp_fortran - A node level benchmark (written with MPI+OpenMP but most communication removed) representing node level work of main 
bottleneck in sigma.cplx.x executable.

kernels/gpp_cpp - Various translations of gpp_fortran to C++, including support for GPUs with OpenMP 4.5 and Kokkos

kernels/chi_summation - A node level benchmark representing the GEMM (and surrounding work) at one of the major bottlenecks in BerkeleyGW

kernels/fft - A node level (threaded) FFT benchmark, representing a bottleneck in BerkeleyGW benchmark


--------------------------
Building Instructions:

1. 
% gunzip source.gz

2. Enter the source directory. And find an appropriate configuration file in the config directory. Copy the file to the root directory and 
rename it arch.mk. Edit the file as appropriate with the locations of FFTW/BLAS/LAPACK/Scalapack on the system. For the purposes of running 
the small benchmark problem, do not compile with HDF5 support (do not specify -DHDF5, under mathflag)

3. 
% make sigma


--------------------------
Running Instructions:

small example

1. Download and unpack additional required input files in the small directory:

% wget http://portal.nersc.gov/project/nerscweb/jdeslip/bgwlargefiles.tar)
% tar -xvf bgwlargefiles.tar 

2. Execute:

For example:

% export OMP_NUM_THREADS=32
% srun -n 1 /path/to/installdir/bin/sigma.cplx.x 

To validate the result compare sigma_hp.log to the example output. In particular the lines:

   n         Emf          Eo           X        SX-X          CH         Sig         Vxc        Eqp0        Eqp1       Znk
  16   -0.877796   -0.877796   -7.805797    4.715920   -5.997245   -9.087122  -12.486998    2.522080    2.157284    0.892703

For Kernels see instructions in each directory

-------------------------
About C++ kernels:

Currently we have ported the GPP kernel of the code with KOKKOS and OpenMP frameworks on KNL, Haswell and GPU architectures.
The code performs complex number computations on a single node.
Fortan natively supports complex numbers unlike C/C++ where we have to use a std::complex class.
The standard complex library overhead limits the performance of C/C++ codes.
Hence we also wrote a Complex class called GPUComplex which we use to perform the complex number computation. 
For GPU's we derive the class from 'double2' vector type from cuda framework. 
The application contains optimized codes for the following implementations along with their respective Makefiles: 


   --  Sequential fortran/C++
   --  Fortran with OpenMP3.0 and OpenMP4.5 using the target directives.
   --  C++ with OpenMP3.0 and OpenMP4.5.
   --  C++ code with Personal Complex.h

Soon to Come : 
   --  C++ with KOKKOS-OpenMP version
   --  C++ with KOKKOS-CUDAUVM version
   --  C++ with KOKKOS-CUDA version
    

Compiling the code : 
    There are seperate Makefiles for each of the implementations with the extentions representing the implementation. 
    The Makefile provided is very basic and will have to be tuned based on the compilers used. 
    They have the necessary flags needed for GCC, xlc and Intel compilers. Modify them according to your environment. 
    For code with GPUComplex and OpenMP4.5, copy the respective code to gppKer_gpuComplex.cpp and use Makefile.GPUComplex to compile it.

Running the code : 
    ./app_implementation.ex {params}

DATA SETS {params}: 
    SMALL : 512 2 8192 20
    Results : 
        achtemp[0] = (-16494907.0769245,82474535.3846157)
        achtemp[1] = (-8577351.67999958,60041461.759995)
        achtemp[2] = (-5230092.48780562,47070832.3902488)

    MEDIUM : 512 2 16384 20
    Results : 
        achtemp[0] = (-66060288.0000103,330301439.99994)
        achtemp[1] = (-34351349.7600068,240459448.320065)
        achtemp[2] = (-20945944.975609,188513504.780453)

    LARGE : 512 2 32768 20
    Results : 
        achtemp[0] = (-264241152.00005,1321205759.99964)
        achtemp[1] = (-137405399.039951,961837793.279783)
        achtemp[2] = (-83783779.9023993,754054019.121537)
