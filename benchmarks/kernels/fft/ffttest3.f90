!! ifort -openmp -xMIC_AVX512 ffttest3.f90 -mkl=parallel -o ffttest3.x -I/usr/common/software/intel/compilers_and_libraries_2016.2.181/linux/mkl/include/fftw/

program ffttest

!  use omp_lib

  implicit none
  
  include 'fftw3.f'
  !include 'fftw3_mkl.f'

  complex(kind((1.0d0,1.0d0))), allocatable :: fftbox(:,:,:,:)
  real(kind(1.0d0)) :: starttime, currenttime
  integer :: Nfft(3),ix,iy,iz,ixr,iyr,izr,num_fft_threads,iret,i,nx,ny,nz,nloop
  integer*8 :: fft_plan = 0
  CHARACTER(len=32) :: arg
  integer :: OMP_GET_NUM_THREADS

  CALL getarg(1, arg)
  READ(arg,*) nx
  CALL getarg(2, arg)
  READ(arg,*) ny
  CALL getarg(3, arg)
  READ(arg,*) nz
  CALL getarg(4, arg)
  READ(arg,*) nloop

  Nfft(1) = nx
  Nfft(2) = ny
  Nfft(3) = nz

  allocate(fftbox(Nfft(1),Nfft(2),Nfft(3),nloop))

  call timget(starttime)

  fftbox = 1

  call timget(currenttime)
  write(6,*) "IO Time",currenttime-starttime

  close(10)

  call timget(starttime)

  call dfftw_init_threads(iret)

!$OMP PARALLEL
      num_fft_threads = OMP_GET_NUM_THREADS()
!$OMP END PARALLEL

  write(6,*) 'Number of threads: ', num_fft_threads

  call dfftw_plan_with_nthreads(num_fft_threads)

  !call dfftw_plan_dft(fft_plan,3,Nfft,fftbox,fftbox,FFTW_FORWARD,FFTW_ESTIMATE)
  !call dfftw_plan_dft_3D(fft_plan,Nfft(1),Nfft(2),Nfft(3),fftbox,fftbox,FFTW_FORWARD,FFTW_ESTIMATE)
  call dfftw_plan_many_dft(fft_plan,3,Nfft,nloop,fftbox,Nfft,1,Nfft(1)*Nfft(2)*Nfft(3),fftbox,Nfft,1,Nfft(1)*Nfft(2)*Nfft(3),FFTW_FORWARD,FFTW_ESTIMATE)

  call timget(currenttime)
  write(6,*) "Plan time",currenttime-starttime

  call timget(starttime)

  call dfftw_execute_dft(fft_plan,fftbox,fftbox)

  call timget(currenttime)
  write(6,*) "Execute Time",currenttime-starttime

  !do iz = 1, Nfft(3)
  !do iy = 1, Nfft(2)
  !do ix = 1, Nfft(1)
  !  write(*,*) fftbox(ix,iy,iz)
  !  !write(*,*) fftbox(iz,iy,ix)
  !enddo
  !enddo
  !enddo

  call dfftw_destroy_plan(fft_plan)

  call dfftw_cleanup_threads()

  stop

end program ffttest

subroutine timget(wall)
  real(kind(1.0d0)) :: wall

  integer :: values(8)

  call date_and_time(VALUES=values)
  wall=((values(3)*24.0d0+values(5))*60.0d0 &
    +values(6))*60.0d0+values(7)+values(8)*1.0d-3

  return
end subroutine timget

